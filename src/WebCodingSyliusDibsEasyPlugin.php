<?php

namespace Webcoding\SyliusDibsEasyPlugin;

use Sylius\Bundle\CoreBundle\Application\SyliusPluginTrait;
use Symfony\Component\HttpKernel\Bundle\Bundle;

final class WebCodingSyliusDibsEasyPlugin extends Bundle
{
    use SyliusPluginTrait;
}

