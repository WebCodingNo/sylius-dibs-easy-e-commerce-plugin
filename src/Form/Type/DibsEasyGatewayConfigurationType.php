<?php

namespace WebCoding\SyliusDibsEasyPlugin\Form\Type;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\Validator\Constraints\NotBlank;

final class DibsEasyGatewayConfigurationType extends AbstractType
{
    /**
     * {@inheritdoc}
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('environment', ChoiceType::class, [
                'choices' => [
                    'webcoding.dibs_easy_plugin.secure' => 'secure',
                    'webcoding.dibs_easy_plugin.sandbox' => 'sandbox',
                ],
                'label' => 'webcoding.dibs_easy_plugin.environment',
            ])
            ->add('secret_key', TextType::class, [
                'label' => 'webcoding.dibs_easy_plugin.secret_key',
                'constraints' => [
                    new NotBlank([
                        'message' => 'webcoding.dibs_easy_plugin.gateway_configuration.secret_key.not_blank',
                        'groups' => ['sylius'],
                    ])
                ],
            ])
            ->add('checkout_key', TextType::class, [
                'label' => 'webcoding.dibs_easy_plugin.checkout_key',
                'constraints' => [
                    new NotBlank([
                        'message' => 'webcoding.dibs_easy_plugin.gateway_configuration.checkout_key.not_blank',
                        'groups' => ['sylius'],
                    ])
                ],
            ])
        ;
    }
}