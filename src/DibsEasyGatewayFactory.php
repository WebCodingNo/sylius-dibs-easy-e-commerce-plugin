<?php

namespace WebCoding\SyliusDibsEasyPlugin;

use WebCoding\SyliusDibsEasyPlugin\Action\AuthorizeAction;
use WebCoding\SyliusDibsEasyPlugin\Action\CancelAction;
use WebCoding\SyliusDibsEasyPlugin\Action\ConvertPaymentAction;
use WebCoding\SyliusDibsEasyPlugin\Action\CaptureAction;
use WebCoding\SyliusDibsEasyPlugin\Action\NotifyAction;
use WebCoding\SyliusDibsEasyPlugin\Action\RefundAction;
use WebCoding\SyliusDibsEasyPlugin\Action\StatusAction;
use Payum\Core\Bridge\Spl\ArrayObject;
use Payum\Core\GatewayFactory;

final class DibsEasyGatewayFactory extends GatewayFactory
{
    /** @var array */
    protected static $REQUIRED_OPTIONS = ['environment', 'secret_key', 'checkout_key'];

    /**
     * {@inheritDoc}
     */
    protected function populateConfig(ArrayObject $config)
    {
        $config->defaults([
            'payum.factory_name' => 'dibs_easy',
            'payum.factory_title' => 'Dibs Easy E-Commerce',

            'payum.action.capture' => new CaptureAction(),
            'payum.action.authorize' => new AuthorizeAction(),
            'payum.action.refund' => new RefundAction(),
            'payum.action.cancel' => new CancelAction(),
            'payum.action.notify' => new NotifyAction(),
            'payum.action.status' => new StatusAction(),
            'payum.action.convert_payment' => new ConvertPaymentAction(),
        ]);

        if (false == $config['payum.api']) {
            $config['payum.default_options'] = [
                'environment' => 'secure',
                'secret_key' => '',
                'checkout_key' => '',
            ];
            $config->defaults($config['payum.default_options']);
            $config['payum.required_options'] = self::$REQUIRED_OPTIONS;

            $config['payum.api'] = function (ArrayObject $config) {
                $config->validateNotEmpty($config['payum.required_options']);

                return new Api((array) $config, $config['payum.http_client'], $config['httplug.message_factory']);
            };
        }
    }
}
